package stack;

public class StackImpl implements Stack {

    StackItem top;
    Object itemm;

    @Override
    public void push(Object item) {
        StackItem topp= new StackItem(item);
        topp.setNext(top);
        top=topp;
    }

    @Override
    public Object pop() {
        itemm=top.getItem();
        top=top.getNext();
        return itemm;
    }

    @Override
    public boolean empty() {
        return (top == null);
    }

}